# cree le base de mon image pour docker hub
FROM minio/mc

# pour copier tout dans l'image, en l'occurence mon script
COPY . .

RUN MKDIR

# defines what command will run my app
CMD ./backup.sh
