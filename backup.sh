#!/bin/bash
set -o errexit
set -o pipefail
set -o nounset

# On récupère ce qui a été entré en argument
dossier=${1:-undefined}

while [ ! -d $dossier ]
do
    # Tant que le dossier entré n'existe pas, en demande en boucle
    echo -n "Dossier inexistant. Essayez encore: "
    read dossier
done

# On créé si besoin le dossier de destination à la racine
mkdir -p ./backups/

# On créé l'archive qu'on place dans /backups
date=$(date '+%d-%m-%Y')
tar cfzP ./backups/backup-$date.tar.gz $dossier

mc config host add minio-test $S3_URL $S3_ACCESS_TOKEN $S3_ACCESS_KEY

mc cp backup-$date.tar.gz minio-test/backups

echo "Le dossier $dossier a été backupé dans /tmp/backups/backup-$date.tar.gz"

echo "allo?"

